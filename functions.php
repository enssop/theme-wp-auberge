<?php

//Ajoute la feuille de style de mon thème
add_action( 'wp_enqueue_scripts', 'auberge_enqueue_styles' );

if (!function_exists('auberge_enqueue_styles')){
    function auberge_enqueue_styles() {

        wp_enqueue_style( 
            'auberge_style', 
            get_stylesheet_uri(),
            array( 'generatepress' ), 
            wp_get_theme()->get('Version') // this only works if you have Version in the style header
        );
    }
}

//Ajoute un script JS
add_action('wp_enqueue_scripts', 'auberge_enqueue_scripts');

if (!function_exists('auberge_enqueue_scripts')){
    function auberge_enqueue_scripts(){
        wp_enqueue_script('auberge_script', //nom du fichier
        get_stylesheet_directory_uri().'/script.js', //emplacement du fichier JS
        array(), //dépendances
        wp_get_theme()->get('Version'), //version
        true //le script est ajouté dans le footer 
        );
    }
}


//Ajoute un type de publication personnalisé
add_action('init', 'auberge_custom_post_type');

if (!function_exists('auberge_custom_post_type')){
    function auberge_custom_post_type(){
        $labels = array();
        $args = array(
            'label'     => 'Chambre', //nom dans l'interface back-office
            'labels'    => $labels,
            'public'    => true, //est accessible publiquement (a une url, est visible sur le front du site, est visible dans le back_office, etc)
            'hierarchical'  => false, //se comporte comme un post et non comme une page
            'exclude_from_search' => false, //les types de publication 'room' seront retournés dans les résultats de recherche
            'publicly_queryable' => true, //visible dans le front du site
            'show_ui'   => true, //visible dans le back-office
            'show_in_menu'  => true, //ajoute un bouton dans le back-office pour gérer les 'room'
            'show_in_nav_menus' => true, //permet d'ajouter les 'room' aux menus de navigation
            'show_in_rest'  => true, //crée une API REST pour les 'room' et permet d'utiliser Gutenberg
            'menu_position' => 5, //où se situe le bouton 'room' dans le menu d'admin
            'menu_icon' => 'dashicons-admin-home', //change l'icone dans l'admin pour une maison
            'capability_type'   => 'post', //mêmes droits utilisateur que les post
            'supports' => array( 'title', 'editor', 'comments', 'revisions', 'trackbacks', 'author', 'excerpt', 'page-attributes', 'thumbnail', 'custom-fields', 'post-formats'),
            'has_archive'   => true, //crée une page d'archive qui liste toutes les 'room'
        );
        register_post_type('room', $args);
    }
}
