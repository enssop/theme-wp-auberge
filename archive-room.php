<?php

/**
 * Ceci est la page d'archive des Chambres de l'auberge
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

get_header(); //affiche le header
?>
<main id="rooms">
    <?php if (have_posts()) { // si il existe des enregistrements 'room'
    ?>
        <h1>Les chambres de l'auberge</h1>
        <?php while (have_posts()) { //affichage pour chaque enregistrement 'room'
            the_post(); ?>

            <article>
            <div>
                <?php the_post_thumbnail(); //affiche l'image mise en avant (featured image) ?>
                <?php //echo get_the_post_thumbnail(); //echo + récupération de l'image mise en avant ?>
            </div>
            <div>
                <h2>
                    <a href="<?php the_permalink(); ?>">
                        <!-- affiche l'url de l'enregistrement 'room' dans la balise a -->
                        <?php the_title(); ?>
                        <!-- affiche le titre de l'enregistrement -->
                    </a>
                </h2>
                <?php the_excerpt(); //affiche l'extrait de l'enregistrement room ?>
                <?php the_content(); //affiche tout le contenu Gutenberg avec sa mise en forme ?>
                <?php $orientation = get_field('orientation');
                if ($orientation) { ?>
                    <p>La chambre est exposée :
                        <?php foreach ($orientation as $o) {
                            echo $o['label'] . ', ';
                        } ?>
                    </p>
                <?php } ?>
            </div>
            </article>
        <?php } ?>
    <?php } ?>
</main>

<?php
get_footer(); //affiche le footer
