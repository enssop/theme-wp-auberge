<?php

/**
 * Affichage de la page d'accueil
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

get_header(); //affichage du header
?>

<div <?php generate_do_attr('content'); ?>>
    <!-- affichage des classes et de la structure de GeneratePress -->
    <main <?php generate_do_attr('main'); ?>>
        <?php
        /**
         * generate_before_main_content hook.
         *
         * @since 0.1
         */
        do_action('generate_before_main_content');

        if (generate_has_default_loop()) {
            while (have_posts()) :

                the_post();

                generate_do_template_part('page');

            endwhile;
        }

        /**
         * generate_after_main_content hook.
         *
         * @since 0.1
         */
        do_action('generate_after_main_content');
        ?>

        <?php

        /**
         * Requête personnalisée pour afficher les enregistrement 'feedback'
         */
        $args = array(
            'post_type'  => 'feedback',
            'posts_per_page' => 4,
        );
        $query = new WP_Query($args);

        if ($query->have_posts()) { ?>
            <section id="feedback">
                <h2>Nos clients nous recommandent</h2>
                <div>
                    <?php while ($query->have_posts()) {
                        $query->the_post(); ?>
                        <article>
                            <h3><?php the_title(); ?></h3>
                            <?php $rating = get_field('rating');
                            if ($rating) {
                                for ($i = 0; $i < $rating; $i++) {
                                    echo "<img class='feedback-rating' src='" . get_stylesheet_directory_uri() . "/star.svg'>";
                                }
                            } ?>
                            <p><?php the_excerpt(); ?></p>
                        </article>
                    <?php } ?>
                </div>
                <a href="<?php echo get_home_url().'/feedback'; ?>">Voir tous les avis</a>
            </section>
        <?php }
        ?>


    </main>
</div>

<?php
/**
 * generate_after_primary_content_area hook.
 *
 * @since 2.0
 */
do_action('generate_after_primary_content_area');

generate_construct_sidebars();

get_footer();
