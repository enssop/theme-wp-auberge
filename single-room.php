<?php 
/**
 * Template pour l'affichage d'un enregistrement 'room'
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header(); ?>

<main id="room">
    <?php if(have_posts()){ ?>

        <a href="<?php echo get_home_url().'/room'; ?>">Voir toutes les chambres de l'auberge</a>
       <?php while (have_posts()){
            the_post();
            ?>

            <h1><?php the_title(); ?></h1>

        <?php }
    } ?>
</main>

<?php
get_footer();
